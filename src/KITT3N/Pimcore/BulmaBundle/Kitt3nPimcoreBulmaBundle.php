<?php

namespace KITT3N\Pimcore\BulmaBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class Kitt3nPimcoreBulmaBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcorebulma/js/pimcore/startup.js'
        ];
    }
}
