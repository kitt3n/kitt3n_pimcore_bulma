pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreBulmaBundle");

pimcore.plugin.Kitt3nPimcoreBulmaBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreBulmaBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreBulmaBundle ready!");
    }
});

var Kitt3nPimcoreBulmaBundlePlugin = new pimcore.plugin.Kitt3nPimcoreBulmaBundle();
